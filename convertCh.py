#!/usr/bin/env python

import os

lines = [x.strip() for x in open("channels.csv").readlines()[1:]]

for line in lines:
	data = line.split(",")
	ieta = data[9]
	iphi = data[10]
	ix = data[11]
	iy = data[12]
	iz = data[13]
	chid = data[17]
	print "Channel({0}, {1}, {2}, {3}, {4}, {5}),".format(chid, ix, iy, iz, ieta, iphi)