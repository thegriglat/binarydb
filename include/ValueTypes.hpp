#ifndef PFG_VALUETYPES_H
#define PFG_VALUETYPES_H
// this file describes common value types for data

#include <string>
#include <sstream>

template <typename T>
struct Result
{
    bool state;
    T result;
    operator bool() const
    {
        return state;
    }

    operator T() const
    {
        return result;
    }

    T operator()() const
    {
        return result;
    }
};

// basic type for real numbers
typedef float Value;

struct ValueRMS
{
    Value mean;
    Value rms;
};

namespace ValueTypes
{

    inline std::string format(const Value value)
    {
        std::ostringstream ss;
        ss << value;
        return ss.str();
    }

    inline std::string format(const ValueRMS value)
    {
        return ValueTypes::format(value.mean) + " +- " + ValueTypes::format(value.rms);
    }
} // namespace ValueTypes

#endif