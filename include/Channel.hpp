#ifndef PFG_CHANNEL_H
#define PFG_CHANNEL_H

/*
fed,
tcc,
tower,
stripintower,
xtalinstrip,
ccu,
vfe,
xtalinvfe,
xtalinccu,
ieta,
iphi,
ix,
iy,
iz,
hashedid,
ic,
cmsswid,
dbid,
ietatt,
iphitt,
tccch,
tccslot,
slbch,
slbslot,
ietagct,
iphigct,
det,
crate

610,37,1,5,5,1,5,5,24,-1,1,-999,-999,-1,30240,1,838861313,1011190001,-1,71,1,5,1,1,10,0,EB-1,S2D04d

*/
/*
struct ChannelFull
{
    int fed;
    int tcc;
    int tower;
    int stripintower;
    int xtalinstrip;
    int ccu;
    int vfe;
    int xtalinvfe;
    int xtalinccu;
    int ieta;
    int iphi;
    int ix;
    int iy;
    int iz;
    int hashedid;
    int ic;
    int cmsswid;
    int dbid;
    int ietatt;
    int iphitt;
    int tccch;
    int tccslot;
    int slbch;
    int slbslot;
    int ietagct;
    int iphigct;
    char det[6];
    char crate[7];
};
*/

#define EMPTYVALUE (-999)

class Channel
{
private:
    int _ix; // iz == 0 => ieta
    int _iy; // iz == 0 => iphi
    int _iz;
    int _channelId;

public:
    Channel(int channelId, int ix, int iy, int iz, int ieta, int iphi) : _channelId(channelId), _ix(ix), _iy(iy), _iz(iz)
    {
        if (ix == EMPTYVALUE && iy == EMPTYVALUE)
        {
            _iz = 0;
            _ix = ieta;
            _iy = iphi;
        };
    }

    int channelId() const
    {
        return _channelId;
    }

    int ix() const
    {
        return (_iz != 0) ? _ix : EMPTYVALUE;
    }

    int iy() const
    {
        return (_iz != 0) ? _iy : EMPTYVALUE;
    }

    int ieta() const
    {
        return (_iz != 0) ? EMPTYVALUE : _ix;
    }

    int iphi() const
    {
        return (_iz != 0) ? EMPTYVALUE : _iy;
    }

    int iz() const
    {
        return _iz;
    }
};

#undef EMPTYVALUE
#endif