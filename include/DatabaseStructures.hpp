#ifndef PFG_DATABASESTRUCTURES_H
#define PFG_DATABASESTRUCTURES_H

#include <iostream>
#include <string>
#include <cstring>

#include "ValueTypes.hpp"
#include "Channel.hpp"

/*
This file describes file format for data
File format:
  Header
  Channel-1
  Channel-2
  ...
  Channel-N

====
All structs should be enumberated to control data versioning
Default value should be typedef-s
*/

// #define PACKED __attribute__((packed))
#define PACKED

// Run info
struct PACKED RunInfo_v1
{
    int run;

    RunInfo_v1(){};
    RunInfo_v1(int _run) : run(_run)
    {
    }

    inline void print()
    {
        std::cout << "Run:\t" << this->run << std::endl;
    }
};

typedef RunInfo_v1 Run;

struct PACKED ChannelInfo_v1
{
    Channel channel;
    ValueRMS pedestal;
    ValueRMS timing;

    inline void print()
    {
        std::cout << "Channel Pedestal:\t" << ValueTypes::format(this->pedestal) << std::endl;
        std::cout << "Channel timing:\t" << ValueTypes::format(this->timing) << std::endl;
    }
};

typedef ChannelInfo_v1 ChannelInfo;

// File header
struct PACKED Header_v1
{
    const int file_version = 1;
    Run run;

    Header_v1(){};
    Header_v1(Run _run) : run(_run){};

    inline void print()
    {
        std::cout << "File version: " << this->file_version << std::endl;
        std::cout << "Run info:" << std::endl;
        this->run.print();
    }
};

typedef Header_v1 Header;

#endif