#include <string>
#include <vector>
#include <fstream>
#include <iostream>

#include "TFile.h"
#include "TTree.h"

#include "ValueTypes.hpp"
#include "DatabaseStructures.hpp"

using namespace std;

const string csvfile = "value_flat.csv";

vector<string> split(const string text, const string delimiter)
{
    vector<string> out;
    out.reserve(7);
    size_t pos;
    string s = text;
    while ((pos = s.find(delimiter)) != std::string::npos)
    {
        out.push_back(s.substr(0, pos));
        s = s.substr(pos + delimiter.length());
    }
    out.push_back(s);
    return out;
}
const Channel *findChannel(int channelId);

int main()
{
    fstream in(csvfile, std::ios::in);
    string line;
    int oldrun = -1;
    int olddataset = -1;
    TFile *rfile = nullptr;
    TTree *tree = nullptr;
    long lineno = 0;
    while (getline(in, line))
    {
        if (lineno % 10000 == 0)
        {
            cout << lineno / 1000 << " K lines dumped ..." << endl;
        }
        vector<string> values = split(line, ",");
        if (values.size() != 7)
        {
            cout << "SKIP: " << line << endl;
            continue;
        }
        int run = atoi(values[0].c_str());
        int dataset = atoi(values[1].c_str());
        if (run == 0)
            continue;
        const Channel *channel = findChannel(atoi(values[2].c_str()));
        if (!channel)
        {
            cout << "Channel " << values[2] << " not found!" << endl;
            continue;
        }
        ChannelInfo chinfo = {
            .channel = *channel,
            .pedestal = {
                .mean = atof(values[4].c_str()),
                .rms = atof(values[3].c_str()),
            },
            .timing = {.mean = atof(values[5].c_str()), .rms = atof(values[6].c_str())}};
        int ix = chinfo.channel.ix();
        int iy = chinfo.channel.iy();
        int iz = chinfo.channel.iz();
        int ieta = chinfo.channel.ieta();
        int iphi = chinfo.channel.iphi();

        if (run != oldrun || dataset != olddataset)
        {
            if (rfile)
            {
                // tree->Write();
                rfile->Write(0, TObject::kOverwrite);
                rfile->Close();
            };
            rfile = new TFile((values[0] + "_" + values[1] + ".root").c_str(), "UPDATE");
            tree = new TTree("channelInfo", "channelInfo");

            tree->Branch("run", &run);
            tree->Branch("ix", &ix);
            tree->Branch("iy", &iy);
            tree->Branch("iz", &iz);
            tree->Branch("ieta", &ieta);
            tree->Branch("iphi", &iphi);
            tree->Branch("pedestal_mean", &(chinfo.pedestal.mean));
            tree->Branch("pedestal_rms", &(chinfo.pedestal.rms));
            tree->Branch("timing_mean", &(chinfo.timing.mean));
            tree->Branch("timing_rms", &(chinfo.timing.rms));
        }
        tree->Fill();
        delete channel;
        oldrun = run;
        olddataset = dataset;
        lineno++;
    }
    // tree->Write();
    rfile->Write(0, TObject::kOverwrite);
    rfile->Close();
}