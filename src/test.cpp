#include "File.hpp"
#include "DatabaseStructures.hpp"

#include <iostream>
using namespace std;

int main()
{
    File<Header, Channel> fp("test.bin");

    Header header(Run(123, 456, 789, "2018B"));
    cout << "start writing" << endl;
    fp.writeHeader(header);
    cout << "writed" << endl;
    cout << "read ..." << endl;
    fp.readHeader().print();
    cout << "write random channels ..." << endl;
    for (int i = 0; i < 74848; ++i)
    {
        Channel ch;
        ch.channel_id = i;
        ch.pedestal.mean = i + 1;
        ch.pedestal.rms = 0.2;
        ch.timing.mean = i - 1;
        ch.timing.rms = 0.5;
        fp.writeChunk(ch);
    }
    cout << "Sort channels ..." << endl;
    fp.sortChunks([](const Channel a, const Channel b) {
        return a.channel_id < b.channel_id;
    });
    /*
    cout << "print channel" << endl;
    for (int i = 0; i < 10; ++i)
        fp.readChunk(i)().print();
*/
    cout << "=====" << endl;
    Channel c;
    c.channel_id = 74847;
    auto res = fp.find(c);
    // auto res = fp.find ([](Channel c){return c.channel_id == 45;});
    if (res)
    {
        res().print();
    }
    else
    {
        cout << "not fould" << endl;
    }
    cout << "sorted? " << fp.isSorted() << endl;
    // fp
    // fp.readHeader().print();
    // for (auto c : fp.filter([](Channel ch) { return ch.channel_id == 1524; }))
    // {
    //     c.print();
    // }
    // auto p = fp.find([](Channel ch) { return ch.channel_id == 1524; });
    // if (p)
    //     p().print();
    // auto q = fp.readChunk(1526);
    // if (q)
    //     q().print();
    cout << "nchannels = " << fp.count() << endl;
}